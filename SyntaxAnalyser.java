package edu.sevgu.kulaginds.spo.compiler.syntax;

import edu.sevgu.kulaginds.spo.compiler.lexical.Lexeme;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Dmitry on 13.05.2016.
 */
public class SyntaxAnalyser {

    private int count_lexemes = 0;
    private int count_nonterms = 0;
    private int nonterms_offset = 0;
    private Token[] grammar;
    private int[][] table;
    private Stack work_stack;
    private Stack out_stack;

    public SyntaxAnalyser(String config_file) throws IOException {
        loadConfig(config_file);
    }

    public SyntaxAnalyser(String config_file, String code_file) throws IOException, SyntaxException {
        loadConfig(config_file);
        Lexeme lexemes = loadCode(code_file);
        if (testCode(lexemes))
            System.out.println("correct");
    }

    protected void loadConfig(String config_file) throws IOException {
        RandomAccessFile f = new RandomAccessFile(config_file, "r");
        count_lexemes = (int)f.readByte();
        count_nonterms = (int)f.readByte();
        int count_rules = (int)f.readByte(),
            count_tokens,
            i,j;
        grammar = new Token[count_rules + 1];
        Token tmp;
        //table = new int[129 + count_nonterms][count_lexemes];
        nonterms_offset = 129 - count_lexemes;
        table = new int[count_lexemes + count_nonterms][count_lexemes];
        // заполняем таблицу ячейками с ошибками
        for (i=0; i<table.length; i++)
            Arrays.fill(table[i], 0xFF);
        // читаем правила грамматики
        for (i=1; i<=count_rules; i++) {
            count_tokens = (int)f.readByte();
            tmp = null;
            for (j=0; j<count_tokens; j++)
                tmp = new Token((0xFF & f.readByte()), tmp);
            grammar[i] = tmp;
        }
        // читаем управляющую таблицу
        for (i=0; i<table.length; i++)
            for (j=0; j<count_lexemes; j++) {
                count_tokens = 0xFF & f.readByte();
                table[i][j] = count_tokens;
            }
            /*if (i <= count_lexemes)
                if (i == count_lexemes)
                    i = 128;
                else
                    for (j=0; j<count_lexemes; j++) {
                        count_tokens = 0xFF & f.readByte();
                        table[i][j] = count_tokens;
                    }
            else
                for (j=0; j<count_lexemes; j++) {
                    count_tokens = 0xFF & f.readByte();
                    table[i][j] = count_tokens;
                }*/
        f.close();
    }

    protected Lexeme loadCode(String code_file) throws IOException {
        RandomAccessFile f = new RandomAccessFile(code_file, "r");
        String code = f.readLine();
        f.close();
        if (code == null)
            throw new RuntimeException("code file is empty");
        String[] lexemes = code.split(",");
        if (lexemes == null || lexemes.length == 0)
            throw new RuntimeException("code file is empty");
        Pattern p = Pattern.compile("L([0-9]+)");
        Matcher m;
        Lexeme start = null,
                current = null, tmp;
        for (int i=0; i<lexemes.length; i++) {
            m = p.matcher(lexemes[i]);
            if (m.find()) {
                tmp = new Lexeme(Integer.parseInt(m.group(1)));
                if (current != null)
                    current.next = tmp;
                current = tmp;
            } else
                throw new RuntimeException("code file is invalid");

            if (i == 0)
                start = current;
        }
        return  start;
    }

    public boolean testCode(Lexeme lexemes) throws SyntaxException {
        boolean is_correct = false;

        if (work_stack == null) {
            work_stack = new Stack();
            work_stack.push(0);
            work_stack.push(129);
        }

        if (out_stack == null)
            out_stack = new Stack();

        int token_id, token_addr, cell;

        do {
            if (lexemes == null)
                throw new SyntaxException(SyntaxException.NO_LEXEMES);

            token_id = (int)work_stack.pop();

            token_addr = token_id;
            if (token_addr > 128)
                token_addr -= nonterms_offset;

            cell = table[token_addr][lexemes.getId()];

            Token token;

            switch (cell) {
                case 0xCC:
                    out_stack.push(token_id);
                    lexemes = lexemes.next;
                    break;
                case 0xDD:
                    is_correct = true;
                    break;
                default:
                    token = grammar[cell];
                    out_stack.push(token_id);
                    while (null != token) {
                        if (token.getId() != 0) // кроме пустого символа
                            work_stack.push(token.getId());
                        token = token.prev;
                    }
                    break;
                case 0xFF: // если ошибка
                    throw new SyntaxException(SyntaxException.ERROR_STATE);
            }
        } while (0xDD != cell);

        return is_correct;
    }

}
