package edu.sevgu.kulaginds.spo.compiler.syntax;

/**
 * Created by Dmitry on 13.05.2016.
 */
public class SyntaxException extends Exception {

    static final String ERROR_STATE = "error state";
    static final String NO_LEXEMES = "no lexemes";

    SyntaxException(String msg) {
        super(msg);
    }

}
