package edu.sevgu.kulaginds.spo.compiler.syntax;

/**
 * Created by Dmitry on 18.05.2016.
 */
public class Token {
    private int id;

    public Token prev;

    public Token(int id, Token prev) {
        this.id = id;
        this.prev = prev;
    }

    public int getId() {
        return id;
    }
}
